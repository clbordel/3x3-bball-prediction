# 3x3-bball-prediction

I built this model with the aim of predicting the win probability of a 3x3 Basketball game (FIBA rules) based on a given game state. The idea for project and the data was provided by Mike Beouy (Twitter as @inpredict) and represents play-by-play data of several 3x3 games from the 2021 Olympic games.

I highly recommend giving Mike a follow if you are interested in statistics or sporty analytics! He was kind enough to give me some pointers during the process and for that I will forever be a fan.

[Original tweet and data from @inpredict](https://twitter.com/inpredict/status/1422609884873433088?s=12)

# Data exploration

We start with standard data exploration. Looking at the `pbp.csv` file we can see there are:

* There are 34 unique games
* There are 5051 individual plays

In the following graph we plot the number of possessions played at each potential pair of scores (i.e. t1 score vs t2 score) to get a good idea of the sample size of each potential state we see in the raw data:

![](output/final/true_per_score_pos.png)

In order to accurately model possession length, ORB % and score values on a play-by-play basis in our simulator, we also look at the distribution of these statistics across our existing dataset:

**ORB/DRB %**

![](output/final/true_orb_drb_per.png)

**Possession lengths**

![](output/final/true_top_v2.png)

**Score distributions**

![](output/final/true_1pt_2pt_shot_per.png)

Some quick google searching showed that these distributions are in line with typical NBA statistics when adjusted for the 3x3 format. With that being said, I felt pretty confident I could accuratley simulate things like rebound percentage, time of possession, etc.

Finally, in order to get a good idea of how our model is performing we also map the true win % (for team 1) for each play in our pbp data to later compare to our predicted percentage wins:

![](output/final/true_win_per_final.png)

# Simulating statistics/Model output

Using the above distributions as a general guide on how we would expect our model to perform we can now simulate our point distributions and time of possession values to ensure they are in line with what we expect.

The following graphs are sample distributions from our simulation methods (ToP and Point sims):

**Possession length simulations (gamma distribution):**

![](output/final/sim_poss_len.png)

**Simulated score distributions:**

![](output/final/sim_poss_length.png)

**The Model**

The model itself requires only the following parameters to run a single simulation:

* team 1 score
* team 2 score
* game clock
* team with possession (1 or 2)

This is all the information needed for the model to simulate one complete game. The following win probabilty matrix represents T1 (our score) on the X axis and T2 Score (Opposing) on the Y axis. Each play in our play-by-play data that contained all availble criteria needed to run a simulation was simualted 10,000 times for a total count of approximatley 50,000,000 total simulations across all available data.
 
![](output/final/pred_win_per_n10000_t4_no_top_final.png)

*Update: 9/10/2021:* In order to more accurately simulate how leading teams change their pace of play towards end game situations, a new factor has been added into simulating time of possession length (credit Mike Beouy for the suggestion and how to implement!). More specifically, when the game clock reaches the final minutes and a team is leading their time of possession is increased by a factor of 1.2-1.4. The following matrix shows the updated win percentage predictions after this is taken into consideration. 

The change itself does not seem to have significant affects on the overall win probability across the majority of scores. However, the effect does seem to be present where a team that is leading has a better change of retaining that lead with the changes as the win percentage is slightly increased, as to be expected.  

![](output/final/pred_win_per_n10000_t4_final.png)

# Run time and performance analysis

Below are the sequential run times for a variety of simulation counts (n=500,n=1000,n=10000). 

* n500 = 49min 41s 17s ± 0 ns per loop (mean ± std. dev. of 1 run, 1 loop each)
* n1000  = 1h 16min 8s ± 0 ns per loop (mean ± std. dev. of 1 run, 1 loop each)
* n10000 = 12h 55min 50s ± 0 ns per loop (mean ± std. dev. of 1 run, 1 loop each)

Sequential run times are listed above. As you can see from the results, a typical session in which each of the ~5,100 plays in our dataset are simulated 10,000 times take approximatley 13 hours. I wasn't **super** happy about that performance - even though the total simulation count is in the millions, I knew I could make it faster.

I was surprised to learn that pandas' groupby.apply() does not have any type of built in parallelization options. As such, I took it upon myself to write a custom wrapper around the groupby.appy() method which would allow me to simultaneously run multiple score group's simulations at once. I limit this to the number of cores available on the machine being used. So, like most things, throwing more money at this will make it perform better!

As you can see below, I was able to nearly cut in half my processing time with this minimal amount of work. With that being said, I feel pretty confident I could peel some more hours off of this fairly easily if I needed to:

* n500 = 23min 41s ± 0 ns per loop (mean ± std. dev. of 1 run, 1 loop each)
* n1000 = 38min 33s ± 0 ns per loop (mean ± std. dev. of 1 run, 1 loop each)
* n10000 = 6h 52min 19s ± 0 ns per loop (mean ± std. dev. of 1 run, 1 loop each)

Here is the win matrix for the parallel implementation of the model:

![](output/final/pred_win_per_n10000_t4_parallel_final.png)
